using Microsoft.VisualStudio.TestTools.UnitTesting;

using Deserialiser;

namespace Tests
{
	[Deserialisable]
	public class Test
	{
		[Order(1)]
		public string test1 { get; set; }

		[Order(2)]
		public int test2 { get; set; }

		[Order(3)]
		public string test3 { get; set; }

		[Order(4)]
		public string test4 { get; set; }
	}

	[Deserialisable]
	public class Test2
	{
		[Order(1)]
		public string test1 { get; set; }

		[Order(2, true)]
		public Test test2 { get; set; }

		[Order(3)]
		public string test3 { get; set; }
	}

	[Deserialisable]
	[Const(3, "test")]
	public class Test3
	{
		[Order(1)]
		public string test1 { get; set; }

		[Order(2)]
		public int test2 { get; set; }

		[Order(4)]
		public string test4 { get; set; }
	}

	[Deserialisable]
	public class Test4 : Test
	{
		[Order(2.1)]
		public string test21 { get; set; }

		[Order(2.2)]
		public int test22 { get; set; }

		[Order(3.1)]
		public string test31 { get; set; }
	}

	[TestClass]
	public class Deserialisation
	{
		[TestMethod]
		public void Test1()
		{
			Test test = Deserialiser<Test>.Deserialise(new object[] { "hi", 12, "test", "bla" });
			Assert.AreEqual("hi", test.test1);
			Assert.AreEqual(12, test.test2);
			Assert.AreEqual("test", test.test3);
			Assert.AreEqual("bla", test.test4);
		}

		[TestMethod]
		public void Test2()
		{
			Test2 test = Deserialiser<Test2>.Deserialise(new object[] { "hi", "hi2", 12, "test", "bla", "213" });
			Assert.AreEqual("hi", test.test1);
			Assert.AreEqual("hi2", test.test2.test1);
			Assert.AreEqual(12, test.test2.test2);
			Assert.AreEqual("test", test.test2.test3);
			Assert.AreEqual("bla", test.test2.test4);
			Assert.AreEqual("213", test.test3);
		}

		[TestMethod]
		public void Test3_1()
		{
			Test3 test = Deserialiser<Test3>.Deserialise(new object[] { "hi", 12, "test", "bla" });
			Assert.AreEqual("hi", test.test1);
			Assert.AreEqual(12, test.test2);
			Assert.AreEqual("bla", test.test4);
		}

		[TestMethod]
		public void Test3_2()
		{
			Assert.ThrowsException<IncorrectValueException>(() => {
				Deserialiser<Test3>.Deserialise(new object[] { "hi", 12, "test2", "bla" });
			});
		}

		[TestMethod]
		public void Test4()
		{
			Test4 test4 = Deserialiser<Test4>.Deserialise(new object[] { "hi", 12, "inherit1", 27, "test", "inherit2", "bla" });
			Assert.AreEqual("hi", test4.test1);
			Assert.AreEqual(12, test4.test2);
			Assert.AreEqual("inherit1", test4.test21);
			Assert.AreEqual(27, test4.test22);
			Assert.AreEqual("test", test4.test3);
			Assert.AreEqual("inherit2", test4.test31);
			Assert.AreEqual("bla", test4.test4);
		}
	}
}
